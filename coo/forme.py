"""Classe de base pour les formes"""

from abc import ABC, abstractmethod

class Forme(ABC):
    @abstractmethod
    def translate(self, dx, dy):
        """Déplace la forme d'un décalage en x et en y"""
        pass
