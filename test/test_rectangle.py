from unittest import TestCase, main
from coo import rectangle2d, point2d

class RectangleTestCase(TestCase):
    def setUp(self):
        orig = point2d.Point2D(1.0, 2.0)
        self.r = rectangle2d.Rectangle2D(orig, 3.0, 4.0)

    def assert_rectangle(self, expectedOrigX, expectedOrigY, expectedFinX, expectedFinY):
        self.assertEqual(self.r.orig.x, expectedOrigX)
        self.assertEqual(self.r.orig.y, expectedOrigY)
        self.assertEqual(self.r.fin.x, expectedFinX)
        self.assertEqual(self.r.fin.y, expectedFinY)

    def test_constructor(self):
        self.assert_rectangle(1.0, 2.0, 4.0, 6.0)

    def test_str(self):
        self.assertEqual(str(self.r), "Rectangle2D(Point2D(1.0, 2.0), Point2D(4.0, 6.0))")

    def test_translate(self):
        self.r.translate(2.0, 3.0)
        self.assert_rectangle(3.0, 5.0, 6.0, 9.0)

if __name__ == '__main__':
    main()
